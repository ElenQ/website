; Global to the file, use (set! lang "lang") to configure in languages
(define lang "en")

; UTILS -----------------------------------------------------------------------
(define (anchored-h level title id)
  `(,(string->symbol (string-append "h" (number->string level)))
     (@ (id ,id))
     ,title
     (a (@ (href ,(string-append "#" id)) (class "anchor")) " ¶" )))


(define (absurl-to-lang url)
  (string-append "/" lang url))

(define (header-link lr text title link active)
  `(a (@ (class ,(string-append (if active "active " " ")
                                "navbar-link link-" lr))
         (href ,link)
         (title ,title))
      ,text))

(define (style href)
  "Make <link> tags to add CSS"
  `(link (@ (rel "stylesheet")
            (href ,href))))

(define (md markdown-text)
  `(@raw ,(md-to-html markdown-text)))

(define (logo-title) `(h1 (@raw ,(file->string "templates/_logo.svg"))))

; BASE ------------------------------------------------------------------------
(define (base title body)
  `((@raw "<!DOCTYPE html>")
    (html
      (@ (lang ,lang))
      (head
        (meta (@ (charset "utf-8")))
        (meta (@ (name "viewport")
                 (content "width=device-width, initial-scale=1")))
        ,(style "/static/css/normalize.css")
        ,(style "/static/css/style.css")
        ,(style "/static/css/fonts.css")
        ,(style "/static/css/extra-style.css")
        (title ,title))
      (body ,body))))


; HEADER-----------------------------------------------------------------------
(define (nav-link l)
  (header-link "left"
               (cdr (assq 'name l))
               (cdr (assq 'title l))
               (cdr (assq 'absurl l))
               (or (assq 'active l) #f)))

(define (lang-link l)
  (header-link "right"
               (cdr (assq 'name l))
               (cdr (assq 'title l))
               (cdr (assq 'absurl l))
               (or (assq 'active l) #f)))

(define (header links langs)
  `(header
     (@ (class mainheader))
     (div (@ (class container))
          (nav (@ (class navbar))
               (label (@ (id hamburger-label)
                         (for "hamburger")
                         (class "navbar-link link-left")) (@raw "&#9776;"))
               (input (@ (id "hamburger") (type checkbox)))

               (div (@ (class navbar-contents))
                    (div (@ (class navbar-left))
                         ,(map nav-link links))

                    (div (@ (class "navbar-right"))
                         ,(map lang-link langs)))))))

