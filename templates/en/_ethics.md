We support free knowledge and we consider our clients must have access to
everything we develop for them so that they can audit and edit it without
restrictions.  That’s why only deliver **free software and free
documentation**.

On the other hand, we believe we all have impact in the society so **we
carefully select the projects we work on**, avoiding projects that we consider
that don't have a good impact in the society: that are incompatible with human
rights, abuse vulnerable collectives, etc.

Also, we have a really strong compromise with the free software movement
because it helps us make our everyday job. For that reason, we spend some of
our effort supporting free software in terms of funds or development time.
