*ElenQ Technology* is a **Research and Development** company. This means we
learn for you and give you the knowledge back in a way you can use it freely,
following our [ethics](#ethics).

We also offer **training and engineering** as secondary services, which means
we can help you with that project that never gets finished because you must
focus on more important things like clients and maintenance.

Our experience is based on **Software and Electronics** but we also have a
background on arts, design and other fields. The fact is that we are engineers
and our main talent is learning anything so don't hesitate to [contact
us](#contact) if you think we can help you. 
