### R&D: Research and Development

We primarily focus on Research and Development. Our main goal is to push our
clients forward technologically speaking.

The service is flexible and can focus on research, making reports about
the state of the art in a certain area or about possible alternatives to solve
an issue, or can be focused on development, delivering a proof-of-concept, a
mockup or a fully-featured solution.

Our commitment is to transfer all the knowledge we adquire during the process,
so our clients are able audit, edit or continue the research in their own. For
that reason, and following our ethics framework, we deliver everything under
a free software license.

### Training

We also offer advanced training in computer science, mostly programming, as a
supplementary service as part of our technological grow model.

The training is aimed to provide our clients a way to improve the technicall
skills of their teams, make them learn a new technology or make newcomers learn
the tools and philosophy of the company, among others. In any case, they ease
to set up a highly-skilled team that deeply understand their job and the tools
they need.

We focus on the transmission of the technical basis in order to encourage
critical and independent thinking that serves as a robust basis to keep
learning and researching.
