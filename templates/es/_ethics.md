Como defensores del conocimiento libre, consideramos que nuestros clientes
tienen que tener acceso a todo lo que realizamos para ellos con el fin de poder
auditarlo y editarlo sin ninguna restricción. Por esa razón, únicamente
**entregamos software y documentación libre**.

Por otro lado, como consideramos que todos tenemos un impacto en la sociedad,
**seleccionamos de manera estricta en qué proyectos trabajamos** evitando
apoyar proyectos que consideramos que van a tener un impacto negativo en la
sociedad: que no cumplen los derechos humanos, que buscan explotar a un grupo
de personas vulnerable, etc.

Además, estamos comprometidos con el movimiento del software libre, ya que nos
permite llevar a cabo nuestro trabajo diario. Por ello, dedicamos parte de
nuestros esfuerzos a colaborar con los proyectos que utilizamos, ya sea de
forma económica o dedicando nuestro tiempo de desarrollo con el fin de mantener
el ecosistema vivo y en buen estado.
