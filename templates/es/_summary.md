*ElenQ Technology* es una empresa de **Investigación y Desarrollo**. Lo que
significa que aprendemos para ti y te damos el conocimiento de vuelta de modo
que puedas usarlo de forma libre, siguiendo nuestro [compromiso
ético](#ethics).

También ofrecemos **formación e ingeniería** como servicios secundarios, para poder
ayudarte con ese proyecto que nunca acaba porque tienes que concentrarte en
cosas más importantes como el mantenimiento o tratar con los clientes.

Nuestras especialidades son el **Software y la Electrónica** pero también tenemos
experiencia en el arte, el diseño y otros campos. La verdad es que somos
ingenieros cuyo mayor talento es aprender, así que no dudes en
[contactarnos](#contact) si crees que podemos ayudarte. 
