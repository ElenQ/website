### I+D: Investigación y Desarrollo

Como equipo de Investigación y Desarrollo que somos, nos dedicamos
principalmente a esa tarea. Ofrecemos nuestros servicios como equipo de I+D
independiente para impulsar tecnológicamente a nuestros clientes.

El servicio es flexible y puede estar más centrado en la investigación,
redactando informes de análisis de situación, estado del arte o posibles
alternativas, o en el desarrollo, creando pruebas de concepto, maquetas o
soluciones que resuelvan los problemas de nuestros clientes.

Hacemos hincapié en una transferencia de real de conocimiento y, siguiendo
nuestro compromiso ético, realizamos la entrega de todo lo desarrollado con
licencias libres, para que el cliente sea libre de auditarlo y editarlo a su
antojo.


### Formación

Ofrecemos formaciones avanzadas en el área de la informática, principalmente
sobre programación como parte de nuestro modelo de impulso tecnológico.

Estas formaciones tienen como objetivo introducir a un equipo de profesionales
a una tecnología que aún no conocen, actualizar a profesionales o formar a
recién llegados a conocer la filosofía de la empresa, entre otros. En cualquier
caso, facilitan la puesta en marcha de equipos eficientes que controlan al
detalle su trabajo.

Estas formaciones se centran en los fundamentos técnicos con el fin de fomentar
el pensamiento crítico e independiente creando así una base de conocimientos
robusta sobre la que seguir avanzando de forma independiente.
